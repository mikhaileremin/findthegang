module.exports = function(grunt) {

    // Project configuration.
    grunt.initConfig({
        pkg: grunt.file.readJSON('package.json'),
        requirejs: {
            findthegang: {
                options: {
	                baseUrl:"./src",
                    mainConfigFile:"src/config.js",
	                name: "boot",
	                out:"./build/boot.js",
                    optimize: "uglify2",
                    skipDirOptimize: true,
                    preserveLicenseComments: false,
                    findNestedDependencies: true
                }
            }

        },
        less: {
	        findthegang: {
		        options: {
			        paths: ["/src/"],
			        compress: true
		        },
		        files: [{
			        expand: true,
			        cwd:    "",
			        src:    "src/**/*.less",
			        ext:    ".css"
		        }]
	        }
        },
    	ngtemplates: {
		    findthegang: {
				cwd: 'src',
				src: 'components/**/*.htm*',
				dest: 'src/templates.js',
				options: {
					bootstrap: function(module, script) {
						return 'define(["app"], function(app) { app.run(["$templateCache", function($templateCache) { ' + script + ' }]);});'
					},
					htmlmin:  {
						collapseWhitespace: true,
						collapseBooleanAttributes: true
					},
					//For production mode we add /src/ prefix to all templates to let whem just work in both dev and prod
					url:function(url) { return "/src/"+url; }
				}
			}
		},
	    //This guy removes all console.log and other non-production stuff (they can be in libs or whereever)
	    strip : {
		    dashboard : {
			    src : 'build/boot.js',
			    dest : 'build/boot.js',
			    options : {
				    nodes : ['console.log']
			    }
		    }
	    },
        clean: [
            "src/templates.js",
            "build/templates.js",
            "build/apps",
            "build/services",
	        "build/styles",
            "build/config.js",
            "build/build.txt"
        ],
        "file-creator": {
            "templates": {
                "src/templates.js": function(fs, fd, done) {
                    fs.writeSync(fd, 'define([], function (a) {return a});');
                    done();
                }
            }
        }
    });
    // Load the plugin
    grunt.loadNpmTasks('grunt-contrib-requirejs');
    grunt.loadNpmTasks('grunt-replace');
    grunt.loadNpmTasks('grunt-contrib-jshint');

	grunt.loadNpmTasks('replace');
	grunt.loadNpmTasks('grunt-strip');

	grunt.loadNpmTasks('grunt-contrib-clean');
    grunt.loadNpmTasks('grunt-file-creator');
    grunt.loadNpmTasks('grunt-contrib-less');
    grunt.loadNpmTasks('grunt-contrib-uglify');
	grunt.loadNpmTasks('grunt-angular-templates');

    grunt.registerTask('force', 'force run', function(status){
        grunt.option('force', status!=='off');
    });

	//grunt.registerTask("build-crm",['less:crm','ngtemplates:crm','requirejs:crm']);
grunt.registerTask("findthegang",['less:findthegang','ngtemplates:findthegang','requirejs:findthegang']);

	grunt.registerTask('build', ["findthegang","clean","file-creator:templates"]);
    // Default task(s).
    grunt.registerTask('default', ['build']);

};
