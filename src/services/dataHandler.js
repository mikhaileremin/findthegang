define([
    "app",
    "angular",
    "services/vk"
], function(app,angular,$){
    "use strict";
    var service = ["$http","vk","$rootScope","$routeParams",function($http,vk,$rootScope,$routeParams){
        var apiPrefix="/";

        var _public= {
	        //Variables
	        myAudioList:[],
	        popularList:[],
	        popularLoaded:false,
	        myLoaded:false,

	        currentAudio:false,
			user:{},
	        loggedIn:false,
	        //Methods
			loadPopular:function(){
				vk('audio.getPopular',{
					only_eng:1
				}).then(function (response) {
						_public.popularList.push.apply(_public.popularList,response)
						_public.popularLoaded=true;
				});
			},
	        loadMyAudio:function(){
		        _public.myLoaded=false;
		        vk('audio.get',{
					owner_id:_public.user.id,
			        count:100
		        }).then(function (response) {
			            _public.myAudioList.splice(0);
				        _public.myAudioList.push.apply(_public.myAudioList,response.items)
				        _public.myLoaded=true;
			        if($routeParams.audioId){
				        _public.setCurrentAudioById($routeParams.audioId);
			        }
		        });
	        },
	        saveCurrentAudio:function(){
		        var found=false;
		        angular.forEach(_public.myAudioList,function(item){
			        if(item.id==_public.currentAudio.id){
				        found=item;
				        angular.extend(item,_public.currentAudio);
			        }
		        })
		        if(found){
			        vk("audio.edit",{
				        audio_id:found.id,
				        owner_id:_public.user.id,
				        artist:found.artist,
				        title:found.title
			        });
		        }

	        },
	        removeAudio:function(audio){
		        var foundAt=-1;
		        vk("audio.delete",{
			        audio_id:audio.id,
			        owner_id:_public.user.id
		        });

		        angular.forEach(_public.myAudioList,function(item,i){
			        if(item.id==audio.id){
				        foundAt=i;
			        }
		        })
		        if(~foundAt){
			        _public.myAudioList.splice(foundAt,1);
		        }
	        },
	        addToMy:function(audio){
		        return vk("audio.add",{
			        audio_id:audio.id,
			        owner_id:audio.owner_id
		        }).then(function(response){
			        audio.id=response;
			        _public.myAudioList.push(audio);
			        _public.loadMyAudio();
		        })
	        },
	        setUser:function(id){
		        _public.user.id=parseInt(id);
		        _public.loggedIn=true;
		        _public.loadMyAudio();
		        _public.loadPopular();
	        },
	        setCurrentAudio:function(audio){
				this.currentAudio=angular.copy(audio);
	        },
	        setCurrentAudioById:function(id){
		        var found=false;
		        angular.forEach(_public.myAudioList,function(item){
			        if(item.id==id){
				        found=item;
			        }
		        })
		        if(found){
			        this.setCurrentAudio(found);
		        }
	        },
	        userHasSong:function(audio){
		        var found=false;
		        angular.forEach(_public.myAudioList,function(item,i){
			        if(item.id==audio.id){
				        found=true;
			        }
		        })

		        return found;
	        }

        }


        return _public;
    } ];
    app.factory("dataHandler", service);
    return service;
});