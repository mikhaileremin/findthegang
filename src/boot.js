"use strict";
require(["config"],function(){
    require([
        'app',
	    'angular',
	    'bootstrap',
	    "components/application/application",

        "components/popular/popular",
        "components/myaudio/myaudio",

	    "components/editaudio/editaudio",
        "templates"
    ], function(app,angular){
        angular.bootstrap(document, ['FindTheGangApp']);
    });
})

