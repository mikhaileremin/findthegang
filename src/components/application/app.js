define([
	// Standard Libs
	'angular',
	"angular-route",
	"bootstrap"
], function (angular) {
	"use strict";
	var app = angular.module('FindTheGangApp', ["ngRoute"]);

	app.constant('VK', VK);

	app.config(['$routeProvider',"$locationProvider",
		function($routeProvider,$locationProvider) {

			$routeProvider.when('/myaudio', {
					templateUrl: '/src/components/myaudio/myaudio.html',
					controller:"myaudioCtrl"
				})
				.when('/popular', {//
					templateUrl: '/src/components/popular/popular.html',
					controller:"popularCtrl"
				})
				.when('/edit/:audioId', {//
					templateUrl: '/src/components/editaudio/editaudio.html',
					controller:"editaudioCtrl"
				})
				.otherwise({
					redirectTo:"/myaudio"
				});

			$locationProvider.hashPrefix('!');
			//$locationProvider.html5Mode({enabled:true,requireBase:false})
			$routeProvider.reloadOnSearch=false;

	}]);

    app.config(['$httpProvider', function($httpProvider) {
        $httpProvider.defaults.headers.post['Content-Type'] = 'application/x-www-form-urlencoded;charset=utf-8';
        /**
         * The workhorse; converts an object to x-www-form-urlencoded serialization.
         * @param {Object} obj
         * @return {String}
         */
        var param = function(obj) {
            var query = '', name, value, fullSubName, subName, subValue, innerObj, i;

            for(name in obj) {
                value = obj[name];

                if(value instanceof Array) {
                    for(i=0; i<value.length; ++i) {
                        subValue = value[i];
                        fullSubName = name + '[' + i + ']';
                        innerObj = {};
                        innerObj[fullSubName] = subValue;
                        query += param(innerObj) + '&';
                    }
                }
                else if(value instanceof Object) {
                    for(subName in value) {
                        subValue = value[subName];
                        fullSubName = name + '[' + subName + ']';
                        innerObj = {};
                        innerObj[fullSubName] = subValue;
                        query += param(innerObj) + '&';
                    }
                }
                else if(value !== undefined && value !== null)
                    query += encodeURIComponent(name) + '=' + encodeURIComponent(value) + '&';
            }
            return query.length ? query.substr(0, query.length - 1) : query;
        };

        // Override $http service's default transformRequest
        $httpProvider.defaults.transformRequest = [function(data) {
            return angular.isObject(data) && String(data) !== '[object File]' ? param(data) : data;
        }];

    }]);


	//Directive to make all title bootstraped
	app.directive('title', ["$timeout",function($timeout) {
		return function(scope, elm, attrs) {
			//let things render first
			$timeout(function(){
				angular.element(elm).tooltip({
					trigger:"hover",
					container:"body",
					placement:attrs.tooltipPlacement||'left auto'
				})
			},10)

			scope.$on("$destroy",function(){
				angular.element(elm).tooltip("destroy");
			})
		};
	}]);

	return app;
});
