define([
    'app',
    'angular',
	"services/vk",
	"services/dataHandler",
	"css!components/application/application"
], function (app) {
    "use strict";
    var directive = ["vk","dataHandler","$location","$window",function (vk,dataHandler,$location,$window) {
        return {
            restrict: "A",
            termimal:true,
            templateUrl: '/src/components/application/application.html',
            controller: ["$scope",function (scope) {
	            scope.loggedIn = false;
	            scope.login = function () {
		            VK.Auth.login(function (response) {
			            scope.$apply(function(){
				            scope.loggedIn = !!response.session;
				            if(scope.loggedIn)
				                dataHandler.setUser(response.session.mid);
			            })
		            }, 8);
	            };

	            scope.logout=function () {
		            VK.Auth.logout(function (response) {
			            scope.$apply(function(){
				            scope.loggedIn = !!response.session;
			            })
			            $window.location.reload();
		            });
	            };


	            scope.refreshAuthStatus=function(){
		            VK.Auth.getLoginStatus(function (response) {
			            scope.loggedIn = !!response.session;

			            if(scope.loggedIn)
			                dataHandler.setUser(response.session.mid);
		            });
	            }
	            scope.refreshAuthStatus();

	            scope.isActive=function(section){
		            if(~$location.path().indexOf("edit") && section=="myaudio"){
						return true;
		            }
					return ~$location.path().indexOf(section)
	            }

            }]
        }
    }]
    app.directive('application', directive);
    return app;
});