define([
    'app',
    'angular',
	"services/vk",
	"css!components/editaudio/editaudio"
], function (app) {
    "use strict";
	"use strict";
	var controller = ["$scope","dataHandler","$routeParams","vk","$location",function (scope,dataHandler,$routeParams,vk,$location) {
		scope.data=dataHandler;


		scope.save=function(){
			dataHandler.saveCurrentAudio();
			$location.path('/myaudio');
		}

		scope.cancel=function(){
			$location.path('/myaudio');
		}
	}]
	app.controller('editaudioCtrl', controller)
	return app;
});