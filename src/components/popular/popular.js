define([
    'app',
    'angular',
	"services/dataHandler",
	"css!components/popular/popular"
], function (app) {
    "use strict";
	"use strict";
	var controller = ["$scope","dataHandler",function (scope,dataHandler) {
		scope.data=dataHandler;

		scope.hasSong=function(audio){
			return dataHandler.userHasSong(audio)
		}

		scope.addAudio=function(audio){
			dataHandler.addToMy(audio)
		}

		scope.isLoggedIn=function(){
			return dataHandler.loggedIn;
		}

	}]
	app.controller('popularCtrl', controller)
	return app;
});