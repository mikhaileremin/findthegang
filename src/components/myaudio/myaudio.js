define([
	'app',
	'angular',
	"services/dataHandler",
	"css!components/myaudio/myaudio"
], function (app) {
	"use strict";
	"use strict";
	var controller = ["$scope","dataHandler","$location",function (scope,dataHandler,$location) {
		scope.data=dataHandler;
		scope.removeAudio=function(audio){
			dataHandler.removeAudio(audio);
		}
		
		scope.edit= function (audio) {
			dataHandler.setCurrentAudio(audio);
			$location.path('/edit/'+audio.id);
		}

		scope.remove=function(audio){
			if(confirm("Уверены что хотите удалить аудиозапись \""+audio.artist+" - "+audio.title+"\"")){
				dataHandler.removeAudio(audio);
			}
		}

		scope.isLoggedIn=function(){
			return dataHandler.loggedIn;
		}

	}]
	app.controller('myaudioCtrl', controller)
	return app;
});