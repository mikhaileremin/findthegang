require.config({
		baseUrl: '/src/',
		paths: {
			libs: "../libs", //Doesn't work
			"styles":"../styles",
            "app": "components/application/app",
			angular: '../libs/angular/angular.min',
			jQuery: '../libs/jquery/jquery-2.0.3.min',
			'bootstrap':'../libs/bootstrap/dist/js/bootstrap',
			'angular-sanitize': '../libs/angular/angular-sanitize',
			'angular-aria': '../libs/angular/angular-aria',
            'angular-touch': '../libs/angular/angular-touch',
			'angular-animate': '../libs/angular/angular-animate',
			'angular-route': '../libs/angular/angular-route'},

        map: {
            '*': {
                'css': 'libs/require-css/css'
            }
        },
		shim: {
			'angular-sanitize': { deps: ['angular']},
			'angular-animate': { deps: ['angular']},
			'angular-route': { deps: ['angular']},
            'angular-touch': { deps: ['angular']},
            'angular-aria': { deps: ['angular']},
			'jQuery': { exports: '$'},
			'angular': { exports: 'angular',deps: ['jQuery']},

		    'bootstrap': {deps: ['jQuery',"css!../libs/bootstrap/dist/css/bootstrap.min"]}
		},
		waitSeconds: 120
	});
